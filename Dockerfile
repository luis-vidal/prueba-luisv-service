FROM adoptopenjdk:11-jre-hotspot
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
LABEL collect_logs_with_filebeat="true" decode_log_event_to_json_object="true"