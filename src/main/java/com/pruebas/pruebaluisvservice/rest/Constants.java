package com.pruebas.pruebaluisvservice.rest;

public final class Constants {
    public static final String V1 = "v1";

    public static final String AVAILABILITY = "availability";
    public static final String CONFIRM = "confirm";
    public static final String DELETE = "delete";
    public static final String USER = "user";

    public static final String URL_V1_AVAILABILITY = V1 + "/" + AVAILABILITY;
    public static final String URL_V1_USER = V1 + "/" + USER;

    private Constants(){}
}
