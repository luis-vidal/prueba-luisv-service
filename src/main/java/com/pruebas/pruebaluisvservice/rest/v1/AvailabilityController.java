package com.pruebas.pruebaluisvservice.rest.v1;

import com.pruebas.pruebaluisvservice.service.AvailabilityService;
import com.pruebas.pruebaluisvservice.dto.AvailabilityRequestDTO;
import com.pruebas.pruebaluisvservice.dto.AvailabilityResponseDTO;
import com.pruebas.pruebaluisvservice.rest.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = Constants.URL_V1_AVAILABILITY)
public class AvailabilityController {

    @Autowired
    private AvailabilityService availabilityService;

    @PostMapping
    @ResponseBody
    private AvailabilityResponseDTO availability(@Valid @RequestBody AvailabilityRequestDTO availabilityRequestDTO) {
        return availabilityService.availability(availabilityRequestDTO);
    }

}
