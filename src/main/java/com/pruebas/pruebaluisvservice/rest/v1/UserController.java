package com.pruebas.pruebaluisvservice.rest.v1;

import com.pruebas.pruebaluisvservice.dto.CreateUserDto;
import com.pruebas.pruebaluisvservice.dto.GetUserDto;
import com.pruebas.pruebaluisvservice.dto.mapper.UserDtoConverter;
import com.pruebas.pruebaluisvservice.rest.Constants;
import com.pruebas.pruebaluisvservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = Constants.URL_V1_USER)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserDtoConverter userDtoConverter;

    @PostMapping("/")
    @ResponseBody
    public ResponseEntity<GetUserDto> newUser(@RequestBody CreateUserDto createUserDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(
                userDtoConverter.convertUserEntityToGetUserDto(userService.newUser(createUserDto)));

    }

}
