package com.pruebas.pruebaluisvservice.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Data
public class AvailabilityResponseDTO {
    List<ActivityDTO> activities;
}
