package com.pruebas.pruebaluisvservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvailabilityRequestDTO {
    @NotEmpty(message = "auth is mandatory")
    String auth;
    @NotEmpty(message = "city is mandatory")
    String city;
    String type;
}
