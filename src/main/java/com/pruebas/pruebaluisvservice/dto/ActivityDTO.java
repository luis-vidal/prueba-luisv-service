package com.pruebas.pruebaluisvservice.dto;

import lombok.Data;

@Data
public class ActivityDTO {
    int id;
    int ttoo;
    String type;
    String city;
}
