package com.pruebas.pruebaluisvservice.dto.mapper;

import com.pruebas.pruebaluisvservice.dto.ActivityDTO;
import com.pruebas.pruebaluisvservice.entity.Activity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ActivityMapper {

    ActivityDTO activityToActivityDTO(Activity activity);

    Activity activityToActivityDTO(ActivityDTO activityDTO);

    List<ActivityDTO> activitiesToActivitiesDTO(List<Activity> activities);

}

