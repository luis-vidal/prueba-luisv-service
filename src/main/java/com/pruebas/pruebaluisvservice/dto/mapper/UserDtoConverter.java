package com.pruebas.pruebaluisvservice.dto.mapper;

import com.pruebas.pruebaluisvservice.dto.GetUserDto;
import com.pruebas.pruebaluisvservice.entity.UserEntity;
import com.pruebas.pruebaluisvservice.entity.enums.UserRoleType;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserDtoConverter {

    public GetUserDto convertUserEntityToGetUserDto(UserEntity user) {
        return GetUserDto.builder()
                .username(user.getUsername())
                .avatar(user.getAvatar())
                .roles(user.getRoles().stream()
                        .map(UserRoleType::name)
                        .collect(Collectors.toSet())
                ).build();
    }

}
