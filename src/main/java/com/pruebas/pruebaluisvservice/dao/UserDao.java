package com.pruebas.pruebaluisvservice.dao;

import com.pruebas.pruebaluisvservice.db.UserEntityRepository;
import com.pruebas.pruebaluisvservice.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class UserDao {

    private final UserEntityRepository userEntityRepository;

    public Optional<UserEntity> findByUsername(String username) {
        return userEntityRepository.findByUsername(username);
    }

    public UserEntity create(UserEntity userEntity) {
        return userEntityRepository.save(userEntity);
    }
}
