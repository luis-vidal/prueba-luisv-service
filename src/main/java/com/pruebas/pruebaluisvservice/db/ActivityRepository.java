package com.pruebas.pruebaluisvservice.db;

import com.pruebas.pruebaluisvservice.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    List<Activity> findByCity(String city);
}
