package com.pruebas.pruebaluisvservice.service;

import com.pruebas.pruebaluisvservice.dto.CreateUserDto;
import com.pruebas.pruebaluisvservice.entity.UserEntity;

import java.util.Optional;

public interface UserService {

    Optional<UserEntity> findByUsername(String username);

    UserEntity newUser(CreateUserDto createUserDto);
}
