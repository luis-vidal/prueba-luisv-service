package com.pruebas.pruebaluisvservice.service.impl;

import com.pruebas.pruebaluisvservice.dao.UserDao;
import com.pruebas.pruebaluisvservice.dto.CreateUserDto;
import com.pruebas.pruebaluisvservice.entity.UserEntity;
import com.pruebas.pruebaluisvservice.entity.enums.UserRoleType;
import com.pruebas.pruebaluisvservice.exception.NewUserWithDifferentPasswordsException;
import com.pruebas.pruebaluisvservice.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<UserEntity> findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public UserEntity newUser(CreateUserDto createUserDto) {

        if (createUserDto.getPassword().contentEquals(createUserDto.getPassword2())) {

            UserEntity userEntity = UserEntity.builder().username(createUserDto.getUsername())
                    .password(passwordEncoder.encode(createUserDto.getPassword()))
                    .avatar(createUserDto.getAvatar())
                    .roles(Stream.of(UserRoleType.USER).collect(Collectors.toSet()))
                    .build();

            try {
                return userDao.create(userEntity);
            } catch (DataIntegrityViolationException ex) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El nombre de usuario ya existe");
            }

        } else {
              throw new NewUserWithDifferentPasswordsException();
        }
    }
}
