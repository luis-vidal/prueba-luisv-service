package com.pruebas.pruebaluisvservice.service.impl;

import com.pruebas.pruebaluisvservice.entity.UserEntity;
import com.pruebas.pruebaluisvservice.service.CustomUserDetailsService;
import com.pruebas.pruebaluisvservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
@RequiredArgsConstructor
public class CustomUserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userService.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username + " no encontrado"));
    }
}
