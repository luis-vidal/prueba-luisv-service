package com.pruebas.pruebaluisvservice.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pruebas.pruebaluisvservice.service.AvailabilityService;
import com.pruebas.pruebaluisvservice.db.ActivityRepository;
import com.pruebas.pruebaluisvservice.dto.AvailabilityRequestDTO;
import com.pruebas.pruebaluisvservice.dto.AvailabilityResponseDTO;
import com.pruebas.pruebaluisvservice.dto.mapper.ActivityMapper;
import com.pruebas.pruebaluisvservice.entity.Activity;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AvailabilityServiceImpl implements AvailabilityService {

    Logger logJson = LogManager.getLogger("CONSOLE_JSON_APPENDER");

    private final ActivityRepository activityRepository;
    private ActivityMapper activityMapper = Mappers.getMapper(ActivityMapper.class);

    @Autowired
    public AvailabilityServiceImpl(
            final ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public AvailabilityResponseDTO availability(AvailabilityRequestDTO availabilityRequestDTO) {
        Gson gson = new GsonBuilder().create();;
        logJson.info(gson.toJson(availabilityRequestDTO));
        AvailabilityResponseDTO availabilityResponseDTO = new AvailabilityResponseDTO();
        List<Activity> activities = activityRepository.findByCity(availabilityRequestDTO.getCity());
        if(!StringUtils.isEmpty(availabilityRequestDTO.getType())) {
            activities = filterByType(availabilityRequestDTO.getType(), activities);
        }
        availabilityResponseDTO.setActivities(
                activityMapper.activitiesToActivitiesDTO(activities));


        logJson.info(gson.toJson(availabilityResponseDTO));
        return availabilityResponseDTO;
    }

    private List<Activity> filterByType(String type, List<Activity> activities) {
        return activities.stream().filter(activity -> type.equals(activity.getType())).collect(Collectors.toList());
    }


}
