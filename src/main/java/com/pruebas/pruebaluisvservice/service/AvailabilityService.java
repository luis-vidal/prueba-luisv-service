package com.pruebas.pruebaluisvservice.service;

import com.pruebas.pruebaluisvservice.dto.AvailabilityRequestDTO;
import com.pruebas.pruebaluisvservice.dto.AvailabilityResponseDTO;

public interface AvailabilityService {
    AvailabilityResponseDTO availability(AvailabilityRequestDTO availabilityRequestDTO);
}
