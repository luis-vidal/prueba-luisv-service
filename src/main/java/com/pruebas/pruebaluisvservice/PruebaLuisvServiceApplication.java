package com.pruebas.pruebaluisvservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PruebaLuisvServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaLuisvServiceApplication.class, args);
    }

}

