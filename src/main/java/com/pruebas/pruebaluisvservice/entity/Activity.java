package com.pruebas.pruebaluisvservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Activity {

    public static final String TYPE_EXCURSION = "E";
    public static final String TYPE_TICKET = "T";

    @Id
    @GeneratedValue
    int id;

    int ttoo;

    String type;

    String city;
}
