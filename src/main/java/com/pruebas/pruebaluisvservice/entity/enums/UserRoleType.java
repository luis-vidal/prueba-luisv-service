package com.pruebas.pruebaluisvservice.entity.enums;

public enum UserRoleType {
    USER, ADMIN
}
