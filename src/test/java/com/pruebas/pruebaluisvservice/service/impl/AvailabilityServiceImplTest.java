package com.pruebas.pruebaluisvservice.service.impl;

import com.pruebas.pruebaluisvservice.db.ActivityRepository;
import com.pruebas.pruebaluisvservice.dto.AvailabilityRequestDTO;
import com.pruebas.pruebaluisvservice.dto.AvailabilityResponseDTO;
import com.pruebas.pruebaluisvservice.entity.Activity;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AvailabilityServiceImplTest {

    private static final String CITY_MADRID = "MAD";
    private static final String CITY_BARCELONA = "BCN";
    private static final int TTOO_TUI = 55;
    private static final int TTOO_KUONI = 66;
    private static final String ACTIVITY_TYPE_TICKET = "T";
    private static final String ACTIVITY_TYPE_EXCURSION = "E";
    private static final String AUTH = "auth";

    @Tested(availableDuringSetup = true)
    AvailabilityServiceImpl availabilityService;

    @Mocked
    @Injectable
    ActivityRepository activityRepository;

    @Test
    void availability_when_type_has_value_expect_filtering_by_type() {

        AvailabilityRequestDTO availabilityRequestDTO = new AvailabilityRequestDTO();
        availabilityRequestDTO.setAuth(AUTH);
        availabilityRequestDTO.setCity(CITY_MADRID);
        availabilityRequestDTO.setType(ACTIVITY_TYPE_TICKET);
        List<Activity> activities = Lists.newArrayList(
                Activity.builder().id(1).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_EXCURSION).city(CITY_MADRID).build(),
                Activity.builder().id(3).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_TICKET).city(CITY_MADRID).build()
        );

        new Expectations(){{
            activityRepository.findByCity(CITY_MADRID);
            result = activities;
        }};

        AvailabilityResponseDTO availabilityResponseDTO = availabilityService.availability(availabilityRequestDTO);

        assertEquals(1, availabilityResponseDTO.getActivities().size());
        assertEquals(3, availabilityResponseDTO.getActivities().get(0).getId());
    }

    @Test
    void availability_when_type_is_null_expect_not_filtering() {

        AvailabilityRequestDTO availabilityRequestDTO = new AvailabilityRequestDTO();
        availabilityRequestDTO.setAuth(AUTH);
        availabilityRequestDTO.setCity(CITY_MADRID);
        availabilityRequestDTO.setType(null);
        List<Activity> activities = Lists.newArrayList(
                Activity.builder().id(1).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_EXCURSION).city(CITY_MADRID).build(),
                Activity.builder().id(3).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_TICKET).city(CITY_MADRID).build()
        );

        new Expectations(){{
            activityRepository.findByCity(CITY_MADRID);
            result = activities;
        }};

        AvailabilityResponseDTO availabilityResponseDTO = availabilityService.availability(availabilityRequestDTO);

        assertEquals(2, availabilityResponseDTO.getActivities().size());
        assertEquals(1, availabilityResponseDTO.getActivities().get(0).getId());
        assertEquals(3, availabilityResponseDTO.getActivities().get(1).getId());
    }

    @Test
    void availability_when_type_is_empty_expect_not_filtering() {

        AvailabilityRequestDTO availabilityRequestDTO = new AvailabilityRequestDTO();
        availabilityRequestDTO.setAuth(AUTH);
        availabilityRequestDTO.setCity(CITY_MADRID);
        availabilityRequestDTO.setType("");
        List<Activity> activities = Lists.newArrayList(
                Activity.builder().id(1).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_EXCURSION).city(CITY_MADRID).build(),
                Activity.builder().id(3).ttoo(TTOO_TUI).type(ACTIVITY_TYPE_TICKET).city(CITY_MADRID).build()
        );

        new Expectations(){{
            activityRepository.findByCity(CITY_MADRID);
            result = activities;
        }};

        AvailabilityResponseDTO availabilityResponseDTO = availabilityService.availability(availabilityRequestDTO);

        assertEquals(2, availabilityResponseDTO.getActivities().size());
        assertEquals(1, availabilityResponseDTO.getActivities().get(0).getId());
        assertEquals(3, availabilityResponseDTO.getActivities().get(1).getId());
    }
}