package com.pruebas.pruebaluisvservice.rest.v1;

import com.pruebas.pruebaluisvservice.dto.AvailabilityRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.pruebas.pruebaluisvservice.rest.Constants.URL_V1_AVAILABILITY;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class AvailabilityControllerIT {

    private static final String CITY_MADRID = "MAD";
    private static final String ACTIVITY_TYPE_TICKET = "T";
    private static final int TTOO_TUI = 55;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void availability_when_city_and_type_expect_filtering_activities() throws Exception {

        mockMvc.perform(post("/" + URL_V1_AVAILABILITY)
                .content(asJsonString(AvailabilityRequestDTO.builder().auth("luis").city(CITY_MADRID).type(ACTIVITY_TYPE_TICKET).build()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.activities", hasSize(1)))
                .andExpect(jsonPath("$.activities[0].id", equalTo(3)))
                .andExpect(jsonPath("$.activities[0].ttoo", equalTo(TTOO_TUI)))
                .andExpect(jsonPath("$.activities[0].city", equalTo(CITY_MADRID)))
                .andExpect(jsonPath("$.activities[0].type", equalTo(ACTIVITY_TYPE_TICKET)));
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
