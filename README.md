# README #

prueba-luisv-service

### POSTMAN ###

A postman file is included in the root of the project 

### Technologies and tools ###
* Rest with Spring
* Bean Validation.
* Mapstructs for mapping DTO - entity.
* Spring Data, JPA & H2
* Lombok
* Jmockit
* Postman
* Swagger
* Docker
* Log4j2
* Sleuth
* log4j2-logstash-layout -> log4j2-ecs-layout

### Skills and good practices ###
* Lambdas
* Unit tests 3As FIRST
* Integration tests
* Extracting methods for readibility
* Spring Autowired in constructors

### Pending of implementation ###
* globalExceptionHandler
* Spring profile
* Logs configuration depends on spring profile